import { readInput } from '../utils/readInput'
import { generateAmphibians } from './utils/generateAmphibians'
import { generateHashOfStep } from './utils/generateHashOfStep'
import { Step } from './types'
import { generateStepsFromStep } from './utils/generateSteps'
import {
  __DEBUG__,
  FINAL_HASH_PART_ONE,
  FINAL_HASH_PART_TWO,
  MAX_DEPTH_PART_ONE,
  MAX_DEPTH_PART_TWO,
} from './constants'
import { generateStepFromHash } from './utils/generateStepFromHash'
import { drawLayout } from './utils/drawLayout'
import { readLine } from '../utils/readLine'

const main = async () => {
  const lines = readInput()

  // Remove two lines for the first part of a task
  const linesForPartOne = [...lines]
  linesForPartOne.splice(3, 2)

  const resultOne = await solveTask(linesForPartOne, MAX_DEPTH_PART_ONE, FINAL_HASH_PART_ONE)
  console.log(`Fist part: ${resultOne}`)

  const resultTwo = await solveTask(lines, MAX_DEPTH_PART_TWO, FINAL_HASH_PART_TWO)
  console.log(`Second part: ${resultTwo}`)
}

const solveTask = async (lines: string[], maxRoomDepth: number, finalHash: string): Promise<number> => {
  const initialStep: Step = [0, generateAmphibians(lines)]
  const [initialCost, initialHash] = generateHashOfStep(initialStep)

  let lowestCost = 0
  const possibleCosts = new Set<number>([initialCost])
  const possibleSteps: Record<number, string[]> = { [initialCost]: [initialHash] }

  const ultimateRecord: Record<string, string> = {}
  const hashRecord: Record<string, number> = {}

  while (!lowestCost) {
    __DEBUG__ && process.stdout.write('\x1Bc')

    const possibleLowestCost = Array.from(possibleCosts).sort((a, b) => a - b)[0]
    const hashForPossibleLowestCost = possibleSteps[possibleLowestCost]?.shift()

    if (!hashForPossibleLowestCost) {
      possibleCosts.delete(possibleLowestCost)
    } else if (hashForPossibleLowestCost === finalHash) {
      lowestCost = possibleLowestCost
    } else {
      __DEBUG__ && drawLayout([possibleLowestCost, hashForPossibleLowestCost], maxRoomDepth)
      const newSteps = generateStepsFromStep(
        generateStepFromHash([possibleLowestCost, hashForPossibleLowestCost]),
        maxRoomDepth
      )

      newSteps.forEach((step) => {
        const [cost, hash] = generateHashOfStep(step)

        // Somehow we can really find the same hash that has a higher cost
        if (hashRecord[hash] && hashRecord[hash] > cost) {
          const oldCost = hashRecord[hash]
          ultimateRecord[hash] = hashForPossibleLowestCost
          hashRecord[hash] = cost
          possibleCosts.add(cost)
          possibleSteps[oldCost].filter((step) => step !== hash)
          possibleSteps[cost] = possibleSteps[cost] || []
          possibleSteps[cost].push(hash)
        }

        if (!ultimateRecord[hash]) {
          __DEBUG__ && console.log([cost, hash])
          ultimateRecord[hash] = hashForPossibleLowestCost
          hashRecord[hash] = cost
          possibleCosts.add(cost)
          possibleSteps[cost] = possibleSteps[cost] || []
          possibleSteps[cost].push(hash)
        }
      })
      __DEBUG__ && (await readLine(''))
    }
  }

  return lowestCost
}

main()
