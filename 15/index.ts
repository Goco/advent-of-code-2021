import { writeFileSync } from 'fs'
import { readInput } from '../utils/readInput'

const getMapValue = (position: [number, number], map: number[][]) => {
  const maxY = map.length
  const maxX = map[0].length

  const [rawX, rawY] = position
  const x = rawX % maxX
  const y = rawY % maxY

  const addX = Math.trunc(rawX / maxX)
  const addY = Math.trunc(rawY / maxY)
  const rawValue = map[y][x] + addX + addY

  return rawValue > 9 ? rawValue - 9 : rawValue
}

const insertIntoQueue = (queue: [number, number][], position: [number, number], distances: Record<string, number>) => {
  const [px, py] = position
  const pKey = `${px}|${py}`
  const pDistance = distances[pKey]

  const oldIndex = queue.findIndex(([qx, qy]) => {
    return qx === px && qy === py
  })
  if (oldIndex !== -1) {
    queue.splice(oldIndex, 1)
  }

  const index = queue.findIndex(([qx, qy]) => {
    const qKey = `${qx}|${qy}`
    const qDistance = distances[qKey]

    return pDistance < qDistance
  })
  if (index === -1) {
    queue.push(position)
  } else {
    queue.splice(index, 0, position)
  }
}

const printAnswer = (border: [number, number], map: number[][], distances: Record<string, number>) => {
  let answer = ''
  for (let y = 0; y < border[1]; ++y) {
    for (let x = 0; x < border[0]; ++x) {
      const key = `${x}|${y}`

      const mapValue = `${getMapValue([x, y], map)}`
      const distance = `${distances[key]}`

      const print = new Array(5 - distance.length).fill(' ').join('') + `${distance}|${mapValue}`
      answer += print
    }
    answer += '\n'
  }

  writeFileSync(`${border[0]}x${border[1]}`, answer)
}

const findShortestPath = (border: [number, number], map: number[][]) => {
  const maxY = border[1] - 1
  const maxX = border[0] - 1
  const distances: Record<string, number> = { '0|0': 0 }

  const queue: [number, number][] = [[0, 0]]

  while (queue.length) {
    const [x, y] = queue.shift()!
    const key = `${x}|${y}`
    const distance = distances[key]
    const destinationDistance = distances[`${maxX}|${maxY}`]

    if (destinationDistance && destinationDistance < distance) {
      break
    }

    const up = x > 0 ? [x - 1, y] : undefined
    const down = x < maxX ? [x + 1, y] : undefined
    const left = y > 0 ? [x, y - 1] : undefined
    const right = y < maxY ? [x, y + 1] : undefined

    const neighbours = [up, down, left, right]
    neighbours.forEach((n) => {
      if (!!n) {
        const [nx, ny] = n
        const nKey = `${nx}|${ny}`
        const nDistance = getMapValue([nx, ny], map)

        const oldDistance = distances[nKey]
        const newDistance = distance + nDistance

        if (typeof oldDistance !== 'number' || oldDistance > newDistance) {
          distances[nKey] = newDistance
          insertIntoQueue(queue, [nx, ny], distances)
        }
      }
    })
  }

  printAnswer(border, map, distances)

  return distances[`${maxX}|${maxY}`]
}

const main = () => {
  const lines = readInput()
  const map = lines.map((line) => line.split('').map((n) => +n))

  const maxY = map.length
  const maxX = map[0].length

  const first = findShortestPath([maxX, maxY], map)
  const second = findShortestPath([maxX * 5, maxY * 5], map)

  console.log({ first, second })
}

main()
