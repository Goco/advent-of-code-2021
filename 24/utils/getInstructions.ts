import { Instruction } from '../types'

export const getInstructions = (lines: string[]): Instruction[] => {
  const rawInstructionsFactory: string[][] = lines.reduce((acc, line) => {
    const [type] = line.split(' ')

    if (type === 'inp') {
      acc.push([])
    } else {
      acc[acc.length - 1].push(line)
    }

    return acc
  }, [])

  return rawInstructionsFactory.map((rawInstructions) => {
    const typeInstruction = rawInstructions[4]
    const [, , rawType] = typeInstruction.split(' ')
    const downNumber = +rawType
    const type = downNumber > 9 ? 'up' : 'down'

    const numberInstruction = rawInstructions[14]
    const [, , rawNumber] = numberInstruction.split(' ')
    const upNumber = +rawNumber

    return [type, type === 'up' ? upNumber : downNumber]
  })
}
