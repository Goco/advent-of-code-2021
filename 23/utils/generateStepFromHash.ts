import { Amphibian, Hash, Step } from '../types'

export const generateStepFromHash = (hash: Hash): Step => {
  const [cost, rawAmphibians] = hash

  return [
    cost,
    rawAmphibians.split('-').map((rawAmphibian) => {
      const [type, position, depth] = rawAmphibian.split('.')

      return [type, +position, +depth] as Amphibian
    }),
  ]
}
