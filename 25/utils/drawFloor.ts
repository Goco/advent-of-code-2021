import { FLOOR } from '../types'

export const drawFloor = (floor: FLOOR, step: number) => {
  console.log(`Step: ${step}`)
  floor.forEach((floorLine) => console.log(floorLine.join('')))
}
