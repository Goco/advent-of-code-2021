import { RebootStep } from '../types'

const getCoordinates = (rawCoordinate: string): [number, number] => {
  const [from, to] = rawCoordinate
    .slice(2)
    .split('..')
    .map((rawNumber) => parseInt(rawNumber))

  return [from, to]
}
export const getRebootStepsFromLines = (lines: string[]): RebootStep[] => {
  return lines.map<RebootStep>((line) => {
    const [operation, rawCuboid] = line.split(' ') as [RebootStep['operation'], string]
    const [rawX, rawY, rawZ] = rawCuboid.split(',')

    return { operation, cuboid: { x: getCoordinates(rawX), y: getCoordinates(rawY), z: getCoordinates(rawZ) } }
  })
}
