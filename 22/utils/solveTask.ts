import { RebootStep } from '../types'
import { getRawRangeFromRawCoordinates } from './getRawRangeFromRawCoordinates'
import { getIndexerFromRawRanges } from './getIndexerFromRawRanges'
import { buildReactor } from './buildReactor'
import { applyStepOnReactor } from './applyStepOnReactor'

export const solveTask = (rebootSteps: RebootStep[]): number => {
  const xRawRange = getRawRangeFromRawCoordinates(rebootSteps.flatMap((step) => step.cuboid.x))
  const yRawRange = getRawRangeFromRawCoordinates(rebootSteps.flatMap((step) => step.cuboid.y))
  const zRawRange = getRawRangeFromRawCoordinates(rebootSteps.flatMap((step) => step.cuboid.z))

  const indexer = getIndexerFromRawRanges(xRawRange, yRawRange, zRawRange)

  const reactor = buildReactor(indexer.range)

  const result = rebootSteps.reverse().map((step) => applyStepOnReactor({ step, reactor, indexer }))
  return result.reduce((acc, curr) => acc + curr, 0)
}
