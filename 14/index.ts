import { readInput } from '../utils/readInput'

const processLines = (lines: string[]) => {
  const polymer = lines.shift()!
  lines.shift()

  const rules = lines.map((line) => {
    const [pair, add] = line.split(' -> ')
    const [p1, p2] = pair.split('')

    return { pair, add: [p1 + add, add + p2] }
  })

  return { polymer, rules }
}

const doStep = (pairsCount: Record<string, number>, rules: { pair: string; add: string[] }[]) => {
  const newPairsCount: Record<string, number> = {}
  rules.forEach(({ pair, add }) => {
    const [add1, add2] = add
    const pairCount = pairsCount[pair]
    if (!!pairCount) {
      newPairsCount[add1] = (newPairsCount[add1] || 0) + pairCount
      newPairsCount[add2] = (newPairsCount[add2] || 0) + pairCount

      delete pairsCount[pair]
    }
  })

  return { ...pairsCount, ...newPairsCount }
}

const countLetters = (firstLetter: string, pairsCount: Record<string, number>) => {
  const letters: Record<string, number> = { [firstLetter]: 1 }
  Object.keys(pairsCount).forEach((pair) => {
    const [, l2] = pair.split('')

    letters[l2] = (letters[l2] || 0) + pairsCount[pair]
  })

  const max = Math.max(...Object.values(letters))
  const min = Math.min(...Object.values(letters))

  return max - min
}

const main = () => {
  const lines = readInput()

  const { polymer, rules } = processLines(lines)
  const firstLetter = polymer[0]
  const polymerChars = polymer.split('')
  let pairsCount = polymerChars.reduce((acc, char2, i) => {
    if (i > 0) {
      const char1 = polymerChars[i - 1]
      acc[`${char1}${char2}`] = (acc[`${char1}${char2}`] || 0) + 1
    }

    return acc
  }, {} as Record<string, number>)

  let first, second
  for (let i = 1; i <= 40; ++i) {
    pairsCount = doStep(pairsCount, rules)
    const result = countLetters(firstLetter, pairsCount)

    if (i === 10) {
      first = result
    }
    if (i === 40) {
      second = result
    }
  }

  console.log({ first, second })
}

main()
