import { Amphibian, AmphibianType } from '../types'
import { AMPHIBIANS } from '../constants'

export const generateAmphibians = (platforms: string[]): Amphibian[] => {
  // iterate over platforms to pick the Amphibians there
  return platforms.flatMap((rawPlatform, platformIndex) => {
    const tiles = rawPlatform.split('')

    // iterate over tiles in a platform
    return tiles
      .map((tile, tileIndex) => {
        if (AMPHIBIANS.some((a) => a === tile)) {
          const type = tile as AmphibianType
          return [type, tileIndex - 1, platformIndex - 1] as Amphibian
        } else {
          return undefined
        }
      })
      .filter((amphibian) => Boolean(amphibian)) as Amphibian[]
  })
}
