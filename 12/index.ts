import { readInput } from '../utils/readInput'

type GraphType = Record<string, string[]>
const processLines = (lines: string[]) => {
  const graph: GraphType = {}
  lines.forEach((line) => {
    const [cave1, cave2] = line.split('-')
    if (cave1 !== 'start') {
      graph[cave2] = graph[cave2] ? [...graph[cave2], cave1] : [cave1]
    }
    if (cave2 !== 'start') {
      graph[cave1] = graph[cave1] ? [...graph[cave1], cave2] : [cave2]
    }
  })
  return graph
}

const doMove = (path: string[], graph: GraphType, withBonus: boolean): string[][] => {
  const currentCave = path.pop()!
  if (currentCave === 'end') {
    return [[...path, currentCave]]
  }

  const isSmall = currentCave === currentCave.toLocaleLowerCase()
  const options = graph[currentCave]

  if (isSmall) {
    const alreadyVisited = path.some((p) => p === currentCave)
    if (alreadyVisited) {
      if (withBonus) {
        withBonus = false
      } else {
        return [[]]
      }
    }
  }

  return options.flatMap((nextCave) => doMove([...path, currentCave, nextCave], graph, withBonus))
}

const main = () => {
  const lines = readInput()

  const graph = processLines(lines)

  const first = doMove(['start'], graph, false).filter((path) => path.length).length
  const second = doMove(['start'], graph, true).filter((path) => path.length).length

  console.log({ first, second })
}

main()
