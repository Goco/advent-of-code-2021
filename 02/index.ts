import { readInput } from '../utils/readInput'

const firstPart = (commands: [string, number][]) => {
  let depth = 0
  let horizontal = 0

  commands.forEach(([command, value]) => {
    switch (command) {
      case 'forward':
        horizontal += +value
        break
      case 'up':
        depth -= +value
        break
      case 'down':
        depth += +value
        break
      default:
        console.log('unknown command! ' + command)
    }
  })

  return depth * horizontal
}

const secondPart = (commands: [string, number][]) => {
  let depth = 0
  let horizontal = 0
  let aim = 0

  commands.forEach(([command, value]) => {
    switch (command) {
      case 'forward':
        horizontal += +value
        depth += +value * aim
        break
      case 'up':
        aim -= +value
        break
      case 'down':
        aim += +value
        break
      default:
        console.log('unknown command! ' + command)
    }
  })

  return depth * horizontal
}

const main = () => {
  const lines = readInput()
  const commands: [string, number][] = lines.map((line) => {
    const [command, rawNumber] = line.split(' ')
    return [command, +rawNumber]
  })

  const first = firstPart(commands)
  const second = secondPart(commands)

  console.log({ first, second })
}

main()
