import { readInput } from '../utils/readInput'

const BINGO_BOARD_SIZE = 5

interface BingoNumber {
  isMarked: boolean
  row: number
  column: number
}

interface BingoBoard {
  isFinished: boolean
  numbers: Record<string, BingoNumber>
  rows: number[]
  columns: number[]
}

const processLines = (lines: string[]) => {
  const bingoBoards: BingoBoard[] = []
  const drawnNumbers: string[] = lines.shift()!.split(',')
  lines.shift()

  do {
    const bingoLines: string[] = []
    while (lines?.[0]?.length) {
      bingoLines.push(lines.shift()!)
    }
    lines.shift()

    const numbers: Record<string, BingoNumber> = {}

    bingoLines.forEach((bingoLine, i) => {
      const bingoNumbers = bingoLine.split(/\s\s|\s/)
      bingoNumbers.forEach((bingoNumber, j) => {
        numbers[bingoNumber] = { isMarked: false, row: i, column: j }
      })
    })

    bingoBoards.push({
      numbers,
      isFinished: false,
      rows: Array(BINGO_BOARD_SIZE).fill(0),
      columns: Array(BINGO_BOARD_SIZE).fill(0),
    })
  } while (!!lines.length)

  return { drawnNumbers, bingoBoards }
}

const playGame = (bingoBoards: BingoBoard[], drawnNumbers: string[]) => {
  let winingBoard: BingoBoard | undefined
  let winingNumber: number | undefined

  let lastBoard: BingoBoard | undefined
  let lastNumber: number | undefined

  let unfinishedGames = bingoBoards.length

  drawnNumbers.every((drawnNumber) => {
    for (let i = 0; i < bingoBoards.length; ++i) {
      const bingoBoard = bingoBoards[i]
      if (bingoBoard.isFinished) {
        continue
      }

      const bingoBoardNumber = bingoBoard.numbers[drawnNumber]
      if (!!bingoBoardNumber) {
        bingoBoardNumber.isMarked = true
        const { row, column } = bingoBoardNumber
        bingoBoard.rows[row] += 1
        bingoBoard.columns[column] += 1

        if (bingoBoard.rows[row] === 5 || bingoBoard.columns[column] === 5) {
          bingoBoard.isFinished = true
          unfinishedGames -= 1

          if (!winingBoard) {
            winingBoard = bingoBoard
            winingNumber = +drawnNumber
          }
          if (unfinishedGames === 0) {
            lastBoard = bingoBoard
            lastNumber = +drawnNumber
            break
          }
        }
      }
    }

    return !lastBoard
  })

  return { winner: { board: winingBoard!, number: winingNumber! }, last: { board: lastBoard!, number: lastNumber! } }
}

const countBoard = (board: BingoBoard, number: number) => {
  const sumOfUnmarkedNumbers = Object.keys(board!.numbers)
    .map((numberKey) => (board!.numbers[numberKey].isMarked ? 0 : +numberKey))
    .reduce((a, b) => a + b, 0)

  return sumOfUnmarkedNumbers * number
}

const main = () => {
  const lines = readInput()

  const { bingoBoards, drawnNumbers } = processLines(lines)

  const { winner, last } = playGame(bingoBoards, drawnNumbers)

  const first = countBoard(winner.board, winner.number)
  const second = countBoard(last.board, last.number)

  console.log({ first, second })
}

main()
