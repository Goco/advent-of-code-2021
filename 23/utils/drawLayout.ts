import { AmphibianType, Hash } from '../types'
import { generateStepFromHash } from './generateStepFromHash'

export const drawLayout = (hash: Hash, maxRoomDepth: number) => {
  const step = generateStepFromHash(hash)
  const [cost, amphibians] = step

  const indexedAmphibians: Record<string, AmphibianType> = amphibians.reduce((acc, amphibian) => {
    const index = `${amphibian[1]}-${amphibian[2]}`

    acc[index] = amphibian[0]

    return acc
  }, {})

  console.log(hash)
  for (let platform = 0; platform <= maxRoomDepth + 2; ++platform) {
    let line = ''
    for (let position = 0; position <= 12; ++position) {
      const isSpace =
        (platform === 1 && position > 0 && position < 12) ||
        ([3, 5, 7, 9].some((i) => i === position) && platform > 0 && platform < maxRoomDepth + 2)
      const index = `${position - 1}-${platform - 1}`
      const tile = indexedAmphibians[index] ?? (isSpace ? ' ' : '#')
      line += tile
    }
    console.log(line)
  }

  console.log(`Cost: ${cost}`)
}
