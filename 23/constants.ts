import { AmphibianType } from './types'

export const __DEBUG__ = false

export const AMPHIBIANS: AmphibianType[] = ['A', 'B', 'C', 'D']

export const AMPHIBIAN_STEPS_MULTIPLIER: Record<AmphibianType, number> = { A: 1, B: 10, C: 100, D: 1000 }

export const ROOM_POSITIONS_INDEXER: Record<AmphibianType, number> = { A: 2, B: 4, C: 6, D: 8 }

export const HALLWAY_POSITIONS = [0, 1, 3, 5, 7, 9, 10]

export const MAX_DEPTH_PART_ONE = 2
export const MAX_DEPTH_PART_TWO = 4

export const FINAL_HASH_PART_ONE = 'A.2.1-A.2.2-B.4.1-B.4.2-C.6.1-C.6.2-D.8.1-D.8.2'
export const FINAL_HASH_PART_TWO =
  'A.2.1-A.2.2-A.2.3-A.2.4-B.4.1-B.4.2-B.4.3-B.4.4-C.6.1-C.6.2-C.6.3-C.6.4-D.8.1-D.8.2-D.8.3-D.8.4'
