import { writeFileSync } from 'fs'
import { readInput } from '../utils/readInput'

type PointMap = Record<string, string>
type Fold = { foldLine: string; foldIndex: number }
const processData = (lines: string[]) => {
  const points: PointMap = {}

  while (lines.length) {
    const point = lines.shift()

    if (point) {
      points[point] = point
    } else {
      break
    }
  }

  const folds: Fold[] = lines.map((line) => {
    const rawFold = line.split(' ')[2]
    const [foldLine, rawIndex] = rawFold.split('=')

    return { foldLine, foldIndex: +rawIndex }
  })

  return { points, folds }
}

const firstPart = (folds: Fold[], points: PointMap) => {
  let result = 0
  folds.forEach(({ foldLine, foldIndex }, i) => {
    Object.keys(points).forEach((point) => {
      const [x, y] = point.split(',').map((p) => +p)
      const foldCoordinates: Record<string, number> = { x, y }

      if (foldCoordinates[foldLine] > foldIndex) {
        delete points[point]
        const newCoordinate = 2 * foldIndex - foldCoordinates[foldLine]
        const key = foldLine === 'x' ? `${newCoordinate},${y}` : `${x},${newCoordinate}`
        points[key] = key
      }
    })

    if (i === 0) {
      result = Object.keys(points).length
    }
  })

  return result
}

const secondPart = (points: PointMap) => {
  const coordinates = Object.keys(points).map((point) => {
    const [x, y] = point.split(',').map((p) => +p)
    return [x, y]
  })
  const [maxX, maxY] = coordinates.reduce(([x, y], point) => [Math.max(x, point[0]), Math.max(y, point[1])], [0, 0])
  const paper = new Array(maxY + 1).fill(0).map(() => new Array(maxX + 1).fill('.'))

  coordinates.forEach(([x, y]) => {
    paper[y][x] = '#'
  })

  const output = paper.map((line) => line.join('')).join('\n')

  writeFileSync('./output.txt', output)
}

const main = () => {
  const lines = readInput()

  const { folds, points } = processData(lines)

  const first = firstPart(folds, points)
  secondPart(points)

  console.log({ first, second: 'output.txt' })
}

main()
