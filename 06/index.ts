import { readInput } from '../utils/readInput'

const loadLanternFish = (fishStages: number[]) => {
  const fishStageCount = Array(9).fill(0)
  fishStages.forEach((stage) => {
    fishStageCount[stage] += 1
  })

  return fishStageCount
}

const simulateLife = (fishStageCount: number[], numberOfDays: number) => {
  for (let i = 0; i < numberOfDays; ++i) {
    const finalStage = fishStageCount.shift() || 0
    fishStageCount[6] += finalStage
    fishStageCount.push(finalStage)
  }

  return fishStageCount.reduce((a, b) => a + b, 0)
}

const main = () => {
  const lines = readInput()

  const fishStageCount = loadLanternFish(lines[0].split(',').map((stage) => +stage))

  const first = simulateLife(fishStageCount, 80)
  const second = simulateLife(fishStageCount, 256 - 80)

  console.log({ first, second })
}

main()
