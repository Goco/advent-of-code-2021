const input = 'target area: x=155..182, y=-117..-67'

const processInput = (input: string) => {
  const ranges = input.match(/([-]*[0-9]+\.\.[-]*[0-9]+)/g)!
  const rangeX = ranges[0]!.split('..').map((n) => +n)
  const rangeY = ranges[1]!.split('..').map((n) => +n)

  return { rangeX, rangeY }
}

const main = () => {
  const targetArea = processInput(input)

  const [minX, maxX] = targetArea.rangeX
  const [minY, maxY] = targetArea.rangeY

  const maxAbsY = Math.max(Math.abs(minY), Math.abs(maxY))
  const maxT = (maxAbsY * (maxAbsY + 1)) / 2

  let maxHeight = 0
  let options = 0

  for (let x = Math.min(0, minX); x <= Math.max(0, maxX); ++x) {
    for (let y = -Math.abs(maxAbsY); y <= Math.abs(maxAbsY); ++y) {
      let velocityX = x
      let velocityY = y

      let positionX = 0
      let positionY = 0

      let isOk = false
      let localMaxHeight = 0

      for (let t = 0; t <= maxT; ++t) {
        positionX += velocityX
        positionY += velocityY

        if (localMaxHeight < positionY) {
          localMaxHeight = positionY
        }

        if (velocityX < 0) {
          velocityX += 1
        }
        if (velocityX > 0) {
          velocityX -= 1
        }
        velocityY -= 1

        if (positionX >= minX && positionX <= maxX && positionY >= minY && positionY <= maxY) {
          isOk = true
        }
      }

      if (isOk) {
        options += 1
        if (localMaxHeight > maxHeight) {
          maxHeight = localMaxHeight
        }
      }
    }
  }

  console.log({ first: maxHeight, second: options })
}

main()
