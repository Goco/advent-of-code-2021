import { readInput } from '../utils/readInput'

const firstPart = (lines: string[]) => {
  const bitCount = Array<number>(lines[0].length).fill(0)
  lines.forEach((line) => {
    bitCount.forEach((count, i) => {
      bitCount[i] += +line[i]
    })
  })

  const linesCount = lines.length
  const max = 2 ** bitCount.length - 1

  const gamma = parseInt(bitCount.map((count) => (count >= linesCount / 2 ? '1' : '0')).join(''), 2)
  const epsilon = max ^ gamma

  return epsilon * gamma
}

const findCorrectNumber = (lines: string[], pickBit: (length: number, count: number) => '1' | '0') => {
  const bitCount = Array(lines[0].length).fill(0)

  let correctLines = lines
  bitCount.forEach((_, i) => {
    if (correctLines.length === 1) {
      return
    } else {
      const count = correctLines.map((line) => +line[i]).reduce((a, b) => a + b, 0)
      const filterBit = pickBit(correctLines.length, count)

      correctLines = correctLines.filter((line) => line[i] === filterBit)
    }
  })

  return correctLines[0]
}

const secondPart = (lines: string[]) => {
  const oxygen = findCorrectNumber(lines, (length, count) => (count >= length / 2 ? '1' : '0'))
  const co2 = findCorrectNumber(lines, (length, count) => (count >= length / 2 ? '0' : '1'))

  return [oxygen, co2].map((n) => parseInt(n, 2)).reduce((a, b) => a * b, 1)
}

const main = () => {
  const lines = readInput()

  const first = firstPart(lines)
  const second = secondPart(lines)

  console.log({ first, second })
}

main()
