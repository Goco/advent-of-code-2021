import { readInput } from '../utils/readInput'

type FlashMap = Record<string, boolean>

const getNeighbours = ([i, j]: [number, number], octopuses: number[][]) => {
  const neighbours: [number, number][] = []

  const maxI = octopuses.length
  for (let x = Math.max(0, i - 1); x <= Math.min(maxI - 1, i + 1); ++x) {
    const maxJ = octopuses[x].length
    for (let y = Math.max(0, j - 1); y <= Math.min(maxJ - 1, j + 1); ++y) {
      if (x !== i || y !== j) {
        neighbours.push([x, y])
      }
    }
  }

  return neighbours
}

const simulateFlash = ([i, j]: [number, number], octopuses: number[][], flashed: Record<string, boolean>) => {
  octopuses[i][j] = 0
  const key = `${i}|${j}`
  flashed[key] = true

  const neighbours = getNeighbours([i, j], octopuses)

  neighbours.forEach(([ni, nj]) => {
    const nKey = `${ni}|${nj}`
    if (!flashed[nKey]) {
      octopuses[ni][nj] += 1
      if (octopuses[ni][nj] > 9) {
        simulateFlash([ni, nj], octopuses, flashed)
      }
    }
  })
}

const simulateGlow = (octopuses: number[][]) => {
  const flashed: FlashMap = {}
  octopuses.forEach((line, i) => {
    line.forEach((_, j) => {
      const key = `${i}|${j}`
      if (!flashed[key]) {
        octopuses[i][j] += 1
        if (octopuses[i][j] > 9) {
          simulateFlash([i, j], octopuses, flashed)
        }
      }
    })
  })

  return Object.keys(flashed).length
}

const main = () => {
  const lines = readInput()
  const octopuses = lines.map((line) => line.split('').map((n) => +n))

  let step = 0
  let flashes = 0
  const answers = { first: 0, second: 0 }

  while (flashes !== 100) {
    step += 1
    flashes = simulateGlow(octopuses)

    if (step === 100) {
      answers.first += flashes
    }
    if (flashes === 100) {
      answers.second = step
    }
  }

  console.log(answers)
}

main()
