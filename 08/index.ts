import { readInput } from '../utils/readInput'

const processInput = (line: string) => {
  const [pattern, output] = line.split(' | ')

  return { pattern: pattern.split(' '), output: output.split(' ') }
}

const UNIQUE_LENGTHS = [2, 3, 4, 7]
const firstPart = (outputs: { output: string[] }[]) => {
  let result = 0
  outputs.forEach(({ output }) => {
    output.forEach((outputNumber) => {
      if (UNIQUE_LENGTHS.some((n) => n === outputNumber.length)) {
        result += 1
      }
    })
  })

  return result
}

type SegmentType = 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g'
type SegmentWiring = Record<SegmentType, SegmentType>
const SegmentMap = {
  abcefg: 0,
  cf: 1,
  acdeg: 2,
  acdfg: 3,
  bcdf: 4,
  abdfg: 5,
  abdefg: 6,
  acf: 7,
  abcdefg: 8,
  abcdfg: 9,
}
const SegmentCount = {
  a: 8,
  b: 6,
  c: 8,
  d: 7,
  e: 4,
  f: 9,
  g: 7,
}

const decodeWiring = (pattern: string[]) => {
  const wiring = {} as SegmentWiring
  const segmentPartsCount = { a: 0, b: 0, c: 0, d: 0, e: 0, f: 0, g: 0 }
  pattern.forEach((p) => {
    p.split('').forEach((s) => {
      segmentPartsCount[s as SegmentType] += 1
    })
  })

  const numberOne = pattern.find((p) => p.length === 2)!.split('')
  const numberSeven = pattern.find((p) => p.length === 3)!.split('')
  const numberFour = pattern.find((p) => p.length === 4)!.split('')

  const segmentB = Object.keys(segmentPartsCount).find(
    (key) => segmentPartsCount[key as SegmentType] === SegmentCount.b
  ) as SegmentType
  const segmentE = Object.keys(segmentPartsCount).find(
    (key) => segmentPartsCount[key as SegmentType] === SegmentCount.e
  ) as SegmentType
  const segmentF = Object.keys(segmentPartsCount).find(
    (key) => segmentPartsCount[key as SegmentType] === SegmentCount.f
  ) as SegmentType

  const segmentA = numberSeven.find((s) => numberOne.every((n) => n !== s)) as SegmentType
  const segmentC = Object.keys(segmentPartsCount).find(
    (key) => segmentPartsCount[key as SegmentType] === SegmentCount.c && key !== segmentA
  ) as SegmentType

  const segmentD = numberFour.find((s) => [segmentB, segmentC, segmentF].every((k) => k !== s)) as SegmentType
  const segmentG = Object.keys(segmentPartsCount).find(
    (key) => segmentPartsCount[key as SegmentType] === 7 && key !== segmentD
  ) as SegmentType

  wiring[segmentA] = 'a'
  wiring[segmentB] = 'b'
  wiring[segmentC] = 'c'
  wiring[segmentD] = 'd'
  wiring[segmentE] = 'e'
  wiring[segmentF] = 'f'
  wiring[segmentG] = 'g'

  return wiring
}

const getNumber =
  (wiring: SegmentWiring) =>
  (outputNumber: string): number => {
    const rawNumber = outputNumber
      .split('')
      .map((o) => wiring[o as SegmentType])
      .sort()
      .join('')

    return SegmentMap[rawNumber as keyof typeof SegmentMap]
  }

const secondPart = (processedLines: { output: string[]; pattern: string[] }[]) => {
  let result = 0

  processedLines.forEach(({ pattern, output }) => {
    const wiring = decodeWiring(pattern)
    result += +output.map(getNumber(wiring)).join('')
  })

  return result
}

const main = () => {
  const lines = readInput()

  const processedValues = lines.map(processInput)

  const first = firstPart(processedValues)
  const second = secondPart(processedValues)

  console.log({ first, second })
}

main()
