import { readInput } from '../utils/readInput'

const countDecreases = (numbers: number[], delimiter = 1) => {
  let result = 0

  numbers.forEach((_, i) => {
    if (i < delimiter) {
      return
    }

    if (numbers[i] > numbers[i - delimiter]) {
      result += 1
    }
  })

  return result
}

const firstPart = (numbers: number[]) => {
  return countDecreases(numbers, 1)
}

const secondPart = (numbers: number[]) => {
  return countDecreases(numbers, 3)
}

const main = () => {
  const lines = readInput()

  const numbers = lines.map((line) => +line)

  const first = firstPart(numbers)
  const second = secondPart(numbers)

  console.log({ first, second })
}

main()
