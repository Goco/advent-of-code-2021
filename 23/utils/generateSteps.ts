import { Amphibian, Step } from '../types'
import { __DEBUG__, AMPHIBIAN_STEPS_MULTIPLIER, HALLWAY_POSITIONS, ROOM_POSITIONS_INDEXER } from '../constants'

export const generateStepsFromStep = (step: Step, maxRoomDepth: number): Step[] => {
  const [cost, amphibians] = step

  return amphibians.flatMap((amphibian, ai) =>
    generateStepsFromAmphibian(
      amphibian,
      amphibians.filter((_, ri) => ai !== ri),
      cost,
      maxRoomDepth
    )
  )
}

const generateStepsFromAmphibian = (
  pickedAmphibian: Amphibian,
  restAmphibians: Amphibian[],
  cost: number,
  maxRoomDepth: number
): Step[] => {
  const [amphibianType, position, depth] = pickedAmphibian

  // check if amphibian is in the room
  if (depth > 0) {
    // check if no one blocks the amphibian in its room
    if (restAmphibians.some((a) => a[1] === position && a[2] < depth)) {
      __DEBUG__ && console.log(`Blocked amphibian "${amphibianType}"`)
      return []
    }

    // check if amphibian isn't already satisfied (is in its room and amphibians that it blocks are also satisfied)
    if (
      ROOM_POSITIONS_INDEXER[amphibianType] === position &&
      restAmphibians.filter((a) => a[1] === position && a[2] > depth).every((a) => a[0] === amphibianType)
    ) {
      __DEBUG__ && console.log(`Satisfied amphibian "${amphibianType}"`)
      return []
    }

    return generateStepsFromRoom(pickedAmphibian, restAmphibians, cost)
  } else {
    // check if the amphibian can move into its room (because that is the only option it can do now)
    const roomPosition = ROOM_POSITIONS_INDEXER[amphibianType]

    //Check if the path to room isn't blocked by amphibian
    const [from, to] = [Math.min(roomPosition, position), Math.max(roomPosition, position)]
    const amphibiansInTheHallway = restAmphibians.some((a) => a[2] === 0 && a[1] > from && a[1] < to)
    if (amphibiansInTheHallway) {
      __DEBUG__ && console.log(`Hallway blocked for amphibian "${amphibianType}"`)
      return []
    }

    // There are still incorrect amphibians in the room
    const amphibiansInTheRoom = restAmphibians.filter((a) => a[1] === roomPosition)
    if (amphibiansInTheRoom.some((a) => a[0] !== amphibianType)) {
      __DEBUG__ && console.log(`Room is not ready for amphibian "${amphibianType}"`)
      return []
    }
    const newDepth = maxRoomDepth - amphibiansInTheRoom.length
    const newCost = +cost + AMPHIBIAN_STEPS_MULTIPLIER[amphibianType] * (Math.abs(position - roomPosition) + newDepth)

    const newAmphibians: Amphibian[] = [...restAmphibians, [amphibianType, roomPosition, newDepth]]

    __DEBUG__ && console.log(amphibianType, position, [roomPosition])
    return [[newCost, newAmphibians]]
  }
}

const generateStepsFromRoom = (pickedAmphibian: Amphibian, restAmphibians: Amphibian[], cost: number): Step[] => {
  const [amphibianType, position, depth] = pickedAmphibian

  const freeHallwayPositions = generateFreeHallwayPositions(position, restAmphibians)

  __DEBUG__ && console.log(amphibianType, position, freeHallwayPositions)

  return freeHallwayPositions.map((newPosition) => {
    const newCost = cost + AMPHIBIAN_STEPS_MULTIPLIER[amphibianType] * (+depth + Math.abs(newPosition - position))
    const newAmphibians: Amphibian[] = [...restAmphibians, [amphibianType, newPosition, 0]]

    return [newCost, newAmphibians]
  })
}

const generateFreeHallwayPositions = (position: number, restAmphibians: Amphibian[]): number[] => {
  // positions of the amphibians in the hallway
  const occupiedPositions = restAmphibians.filter((a) => a[2] === 0).map((a) => a[1])

  // the closest obstruction to the left
  const closestOccupiedPositionLeft = Math.max(...occupiedPositions.filter((p) => p < position))

  // the closest obstruction to the left
  const closestOccupiedPositionRight = Math.min(...occupiedPositions.filter((p) => p > position))

  return HALLWAY_POSITIONS.filter(
    (roomPosition) => roomPosition > closestOccupiedPositionLeft && roomPosition < closestOccupiedPositionRight
  )
}
