import { readInput } from '../utils/readInput'

const firstPart = (lines: string[]) => {
  let result = 0

  return result
}

const secondPart = (lines: string[]) => {
  let result = 0

  return result
}

const main = () => {
  const lines = readInput()

  const first = firstPart(lines)
  const second = secondPart(lines)

  console.log({ first, second })
}

main()
