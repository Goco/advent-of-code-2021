import { readInput } from '../utils/readInput'

type PacketLiteral = {
  version: number
  type: 4
  literal: number
}

type PacketOperator = {
  version: number
  type: number
  lengthType: '0' | '1'
  subPackets: Packet[]
}

type Packet = PacketLiteral | PacketOperator

const processPacket = (bits: string[]): Packet => {
  const versionBits = bits.splice(0, 3)
  const version = parseInt(versionBits.join(''), 2)

  const typeBits = bits.splice(0, 3)
  const type = parseInt(typeBits.join(''), 2)

  if (type === 4) {
    let continueBit: string[] = []
    let literalBits: string[] = []
    do {
      continueBit = bits.splice(0, 1)
      const literalPartBits = bits.splice(0, 4)

      literalBits.push(...literalPartBits)
    } while (continueBit[0] !== '0')
    const literal = parseInt(literalBits.join(''), 2)

    return { type, version, literal }
  }

  const lengthTypeBit = bits.splice(0, 1)
  const lengthType = lengthTypeBit[0] === '0' ? '0' : '1'

  const subPackets: Packet[] = []
  if (lengthType === '0') {
    const lengthOfSubPacketsBits = bits.splice(0, 15)
    const lengthOfSubPackets = parseInt(lengthOfSubPacketsBits.join(''), 2)
    const subPacketsBits = bits.splice(0, lengthOfSubPackets)

    while (subPacketsBits.length) {
      subPackets.push(processPacket(subPacketsBits))
    }
  }

  if (lengthType === '1') {
    const numberOfSubPacketsBits = bits.splice(0, 11)
    const numberOfSubPackets = parseInt(numberOfSubPacketsBits.join(''), 2)

    for (let i = 0; i < numberOfSubPackets; ++i) {
      subPackets.push(processPacket(bits))
    }
  }

  return { type, version, lengthType, subPackets }
}

const firstPart = (packet: Packet): number => {
  const packetOperator = packet as PacketOperator
  const { version, subPackets = [] } = packetOperator
  const subPacketsCount = subPackets.map((sp) => firstPart(sp)).reduce((a, b) => a + b, 0)
  return version + subPacketsCount
}

const secondPart = (packet: Packet): number => {
  if (packet.type === 4) {
    const packetLiteral = packet as PacketLiteral
    return packetLiteral.literal
  }

  const packetOperator = packet as PacketOperator
  const { type, subPackets } = packetOperator
  const subPacketsValues = subPackets.map((sp) => secondPart(sp))

  switch (type) {
    case 0:
      return subPacketsValues.reduce((a, b) => a + b, 0)
    case 1:
      return subPacketsValues.reduce((a, b) => a * b, 1)
    case 2:
      return Math.min(...subPacketsValues)
    case 3:
      return Math.max(...subPacketsValues)
    case 5:
      return subPacketsValues[0] > subPacketsValues[1] ? 1 : 0
    case 6:
      return subPacketsValues[0] < subPacketsValues[1] ? 1 : 0
    case 7:
      return subPacketsValues[0] === subPacketsValues[1] ? 1 : 0
    default:
      return 0
  }
}

const main = () => {
  const lines = readInput()
  const bitStream = lines[0]
  const bits = bitStream
    .split('')
    .map((hex) => parseInt(hex, 16).toString(2).padStart(4, '0'))
    .join('')
    .split('')

  const packet = processPacket(bits)

  const first = firstPart(packet)
  const second = secondPart(packet)

  console.log({ first, second })
}

main()
