import { readInput } from '../utils/readInput'
import { getInstructions } from './utils/getInstructions'
import { Data } from './types'
import { processInstruction } from './utils/processInstruction'

const main = () => {
  const lines = readInput()
  const instructions = getInstructions(lines)

  const results: number[] = []
  let monad = 10000000

  while (monad > 1111111) {
    --monad
    const digits = String(monad)
      .split('')
      .map((n) => +n)

    if (digits.some((d) => d === 0)) {
      continue
    }

    const data: Data = { z: 0, result: [] }
    const correctResult = instructions.every(processInstruction(digits, data))

    if (correctResult) {
      results.push(+data.result.map((n) => `${n}`).join(''))
    }
  }
  const sortedResults = results.sort()

  console.log('First part', sortedResults[sortedResults.length - 1])
  console.log('Second part', sortedResults[0])
}

main()
