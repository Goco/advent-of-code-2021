export type Instruction = ['up' | 'down', number]

export type Data = { z: number; result: number[] }
