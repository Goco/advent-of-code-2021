import { Range, Reactor, THREE_D } from '../types'

export const buildReactor = (range: THREE_D<Range>): Reactor =>
  Array.from({ length: range.x.length }).map(() =>
    Array.from({ length: range.y.length }).map(() => Array.from({ length: range.z.length }).map(() => false))
  )
