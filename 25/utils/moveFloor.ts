import { FLOOR, HERD_TYPE } from '../types'

export const moveFloor = (floor: FLOOR) => {
  const blockedTilesRecord = { v: new Set<string>(), '>': new Set<string>() }
  const X = floor.length
  const Y = floor[0].length

  let cucumberMoved = false

  const moveFloorForHerd = (herdType: HERD_TYPE) => {
    const blockedTiles = blockedTilesRecord[herdType]

    floor.forEach((platform, x) => {
      platform.forEach((tile, y) => {
        if (tile === herdType && !blockedTiles.has(`${x}-${y}`)) {
          const newX = tile === 'v' ? (x + 1) % X : x
          const newY = tile === '>' ? (y + 1) % Y : y

          if (floor[newX][newY] === '.' && !blockedTiles.has(`${newX}-${newY}`)) {
            cucumberMoved = true
            blockedTiles.add(`${x}-${y}`)
            blockedTiles.add(`${newX}-${newY}`)
            floor[newX][newY] = tile
            floor[x][y] = '.'
          }
        }
      })
    })
  }

  moveFloorForHerd('>')
  moveFloorForHerd('v')

  return cucumberMoved
}
