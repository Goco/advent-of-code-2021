export type AmphibianType = 'A' | 'B' | 'C' | 'D'

/**
 * Represents a tuple containing information about an amphibian.
 *
 * The tuple contains:
 * 1. AmphibianType - The type of the amphibian.
 * 2. number - The index of a position of the amphibian.
 * 3. number - The depth of a room (0 for hallway, 1-2 - part one, 1-4 - part two)
 *
 * This type is used to provide structured data about amphibian creatures.
 */
export type Amphibian = [AmphibianType, number, number]

/**
 *
 * The tuple contains:
 * 1. Amphibian[] - an array of the Amphibians and their positions.
 * 2. number - a cost it took to get to this layout.
 */
export type Step = [number, Amphibian[]]

/**
 * Simplified deterministic view of a Step
 */
export type Hash = [number, string]
