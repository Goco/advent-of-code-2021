import { Cuboid, Indexer, Reactor, RebootStep, Transformer3D } from '../types'

export interface ApplyStepOnReactorProps {
  step: RebootStep
  reactor: Reactor
  indexer: Indexer
}

export const applyStepOnReactor = ({ step, reactor, indexer }: ApplyStepOnReactorProps): number => {
  const { operation, cuboid } = step
  const { range, transformer } = indexer
  const {
    x: [xFrom, xTo],
    y: [yFrom, yTo],
    z: [zFrom, zTo],
  } = getIndicesFromCuboid(cuboid, transformer)

  let activeCubesCount = 0
  for (let x = xFrom; x <= xTo; ++x) {
    const xMultiplier = range.x[x]

    for (let y = yFrom; y <= yTo; ++y) {
      const yMultiplier = range.y[y]

      for (let z = zFrom; z <= zTo; ++z) {
        const zMultiplier = range.z[z]
        const alreadyActive = reactor[x][y][z]
        reactor[x][y][z] = true

        // console.log(xMultiplier * yMultiplier * zMultiplier)

        if (alreadyActive || operation === 'off') {
        } else {
          activeCubesCount += xMultiplier * yMultiplier * zMultiplier
        }
      }
    }
  }

  return activeCubesCount
}

const getIndicesFromCuboid = (cuboid: Cuboid, transformer: Transformer3D): Cuboid => {
  const xFrom = transformer.x[cuboid.x[0]]
  const xTo = transformer.x[cuboid.x[1]]

  const yFrom = transformer.y[cuboid.y[0]]
  const yTo = transformer.y[cuboid.y[1]]

  const zFrom = transformer.z[cuboid.z[0]]
  const zTo = transformer.z[cuboid.z[1]]

  if ([xFrom, xTo, yFrom, yTo, zFrom, zTo].some((value) => isNaN(value))) {
    console.log('ERROR!')
  }

  return {
    x: [xFrom, xTo],
    y: [yFrom, yTo],
    z: [zFrom, zTo],
  }
}
