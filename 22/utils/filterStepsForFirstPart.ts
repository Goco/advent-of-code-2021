import { RebootStep } from '../types'

export const filterStepsForFirstPart = (rebootSteps: RebootStep[]): RebootStep[] =>
  rebootSteps.filter((step) => {
    const [xFrom, xTo] = step.cuboid.x
    const [yFrom, yTo] = step.cuboid.y
    const [zFrom, zTo] = step.cuboid.z

    return [xFrom, xTo, yFrom, yTo, zFrom, zTo].every((coord) => coord >= -50 && coord <= 50)
  })