import { Indexer, Range, Transformer } from '../types'
import { visualizeIndexer } from '../helpers/visualizeIndexer'

export const getIndexerFromRawRanges = (
  xRawRange: number[],
  yRawRange: number[],
  zRawRange: number[],
  visualize: boolean = false
): Indexer => {
  const { transformer: transformerX, range: rangeX } = getTransformerAndRangeFromRange(xRawRange)
  const { transformer: transformerY, range: rangeY } = getTransformerAndRangeFromRange(yRawRange)
  const { transformer: transformerZ, range: rangeZ } = getTransformerAndRangeFromRange(zRawRange)

  const indexer: Indexer = {
    transformer: {
      x: transformerX,
      y: transformerY,
      z: transformerZ,
    },
    range: {
      x: rangeX,
      y: rangeY,
      z: rangeZ,
    },
  }

  if (visualize) {
    visualizeIndexer(indexer)
  }

  return indexer
}

type ReducerType = { transformer: Transformer; range: Range; globalIndex: number }

const getTransformerAndRangeFromRange = (rawRange: number[]): { transformer: Transformer; range: Range } => {
  const { transformer, range } = rawRange.reduce(
    ({ transformer, range, globalIndex }, value, index) => {
      if (index) {
        const prevValue = rawRange[index - 1]
        const difference = value - prevValue - 1
        if (difference > 0) {
          range.push(difference)
          ++globalIndex
        }
      }

      transformer[value] = globalIndex
      range.push(1)
      ++globalIndex

      return { transformer, range, globalIndex }
    },
    { transformer: {}, range: [], globalIndex: 0 } as ReducerType
  )

  return { transformer, range }
}
