import { FLOOR, FLOOR_TILE } from '../types'

export const getFloorFromLines = (lines: string[]): FLOOR => lines.map((line) => line.split('') as FLOOR_TILE[])
