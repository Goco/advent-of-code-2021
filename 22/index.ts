import { readInput } from '../utils/readInput'

import { getRebootStepsFromLines } from './utils/getRebootStepsFromLines'
import { solveTask } from './utils/solveTask'
import { filterStepsForFirstPart } from './utils/filterStepsForFirstPart'

const main = () => {
  const lines = readInput()

  const rebootSteps = getRebootStepsFromLines(lines)

  const rebootStepsForFirstPart = filterStepsForFirstPart(rebootSteps)

  const solution1 = solveTask(rebootStepsForFirstPart)
  console.log(`Solution one: ${solution1}`)

  const solution2 = solveTask(rebootSteps)
  console.log(`Solution two: ${solution2}`)
}

main()
