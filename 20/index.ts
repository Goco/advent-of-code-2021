import { readInput } from '../utils/readInput'

const processLines = (lines: string[]) => {
  const imageEnhancer = lines.shift()!
  lines.shift()

  const map = lines.join('\n')

  return { imageEnhancer, map }
}

const mapZoom = (map: string, emptySymbol = '.') => {
  const mapLines = map.split('\n')
  const lineHeight = mapLines[0].length

  const emptyLine = new Array(lineHeight).fill(emptySymbol).join('')
  mapLines.unshift(emptyLine)
  mapLines.push(emptyLine)

  return mapLines.map((line) => `${emptySymbol}${line}${emptySymbol}`).join('\n')
}
const mapEnhance = (imageEnhancer: string, map: string, emptySymbol = '.') => {
  const mapEnhanced: string[] = []
  const mapPoints = map.split('\n').map((line) => line.split(''))

  mapPoints.forEach((line, i) => {
    let mapLine = ''
    line.forEach((point, j) => {
      const maxI = mapPoints.length
      const maxJ = line.length

      let binaryIndex = ''
      for (let y = i - 1; y <= i + 1; ++y) {
        for (let x = j - 1; x <= j + 1; ++x) {
          const isInMap = x >= 0 && y >= 0 && x < maxJ && y < maxI
          binaryIndex += isInMap ? mapPoints[y][x] : emptySymbol
        }
      }

      const index = parseInt(
        binaryIndex
          .split('')
          .map((i) => (i === '#' ? '1' : '0'))
          .join(''),
        2
      )
      mapLine += imageEnhancer[index]
    })
    mapEnhanced.push(mapLine)
  })

  return mapEnhanced.join('\n')
}

const doEnhancement = (imageEnhancer: string, map: string) => {
  let counter = 0

  let enhancedMap = map
  let first, second
  while (counter < 50) {
    const emptySymbol = counter % 2 ? '#' : '.'
    enhancedMap = mapEnhance(imageEnhancer, mapZoom(enhancedMap, emptySymbol), emptySymbol)

    counter += 1
    if (counter === 2) {
      first = enhancedMap.split('').reduce((acc, point) => acc + (point === '#' ? 1 : 0), 0)
    }
    if (counter === 50) {
      second = enhancedMap.split('').reduce((acc, point) => acc + (point === '#' ? 1 : 0), 0)
    }
  }

  return { first, second }
}

const main = () => {
  const lines = readInput()

  const { imageEnhancer, map } = processLines(lines)
  const answer = doEnhancement(imageEnhancer, map)
  console.log(answer)
}

main()
