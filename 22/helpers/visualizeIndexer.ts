import { Indexer, Transformer } from '../types'

export const visualizeIndexer = (indexer: Indexer) => {
  const { transformer, range } = indexer
  const reversedTransformerX = reverseTransformer(transformer.x)
  const reversedTransformerY = reverseTransformer(transformer.y)
  const reversedTransformerZ = reverseTransformer(transformer.z)

  console.log('Dimension X:')
  range.x.forEach((x, i) => {
    const value = reversedTransformerX[i] ?? '---'
    console.log(`[${prettyPrint(i)}] - ${prettyPrint(x)} - (${prettyPrint(value)})`)
  })
  console.log('')

  console.log('Dimension Y:')
  range.y.forEach((y, i) => {
    const value = reversedTransformerY[i] ?? '---'
    console.log(`[${prettyPrint(i)}] - ${prettyPrint(y)} - (${prettyPrint(value)})`)
  })
  console.log('')

  console.log('Dimension Z:')
  range.z.forEach((z, i) => {
    const value = reversedTransformerZ[i] ?? '---'
    console.log(`[${prettyPrint(i)}] - ${prettyPrint(z)} - (${prettyPrint(value)})`)
  })
}

const reverseTransformer = (transformer: Transformer): Transformer => {
  return Object.entries(transformer).reduce((acc, [key, value]) => {
    return { ...acc, [value]: +key }
  }, {})
}

const prettyPrint = (value: string | number, size: number = 3) => {
  const valueString = value.toString()
  const padding = ' '.repeat(size - valueString.length)
  return `${padding}${valueString}`
}
