import { readInput } from '../utils/readInput'

enum SymbolEnum {
  '(' = ')',
  '[' = ']',
  '{' = '}',
  '<' = '>',
  ')' = '(',
  ']' = '[',
  '}' = '{',
  '>' = '<',
}
type SymbolType = keyof typeof SymbolEnum

enum ErrorTableEnum {
  ')' = 3,
  ']' = 57,
  '}' = 1197,
  '>' = 25137,
}
type ErrorSymbolType = keyof typeof ErrorTableEnum

enum AutocompleteTableEnum {
  ')' = 1,
  ']' = 2,
  '}' = 3,
  '>' = 4,
}
type AutocompleteSymbolType = keyof typeof AutocompleteTableEnum

const OPENING_SYMBOLS = ['(', '[', '{', '<']

type ProcessedLineType = { stack: string[]; corruptedSymbol?: string }
const processLines = (lines: string[]): ProcessedLineType[] => {
  return lines.map((line) => {
    const stack: string[] = []
    const corruptedSymbol = line.split('').find((symbol) => {
      if (OPENING_SYMBOLS.some((os) => os === symbol)) {
        stack.unshift(SymbolEnum[symbol as SymbolType])
      } else {
        const closingSymbol = stack[0]!
        if (closingSymbol === symbol) {
          stack.shift()
        } else {
          return true
        }
      }
    })
    return { stack, corruptedSymbol }
  })
}

const firstPart = (processedLines: ProcessedLineType[]) => {
  return processedLines
    .filter(({ corruptedSymbol }) => !!corruptedSymbol)
    .reduce((acc, { corruptedSymbol }) => acc + ErrorTableEnum[corruptedSymbol as ErrorSymbolType], 0)
}

const secondPart = (processedLines: ProcessedLineType[]) => {
  const sortedScore = processedLines
    .filter(({ corruptedSymbol }) => !corruptedSymbol)
    .map(({ stack }) => {
      return stack.reduce((score, symbol) => {
        return 5 * score + AutocompleteTableEnum[symbol as AutocompleteSymbolType]
      }, 0)
    }, 0)
    .sort((a, b) => a - b)

  return sortedScore[Math.trunc(sortedScore.length / 2)]
}

const main = () => {
  const lines = readInput()

  const processedLines = processLines(lines)

  const first = firstPart(processedLines)
  const second = secondPart(processedLines)

  console.log({ first, second })
}

main()
