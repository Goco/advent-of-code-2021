import { readInput } from '../utils/readInput'
import { getFloorFromLines } from './utils/getFloorFromLines'
import { drawFloor } from './utils/drawFloor'
import { moveFloor } from './utils/moveFloor'
import { readLine } from '../utils/readLine'

const __DEBUG__ = true

const main = async () => {
  const lines = readInput()
  const floor = getFloorFromLines(lines)

  let cucumberMoved = true
  let step = 0

  while (cucumberMoved) {
    __DEBUG__ && process.stdout.write('\x1Bc')

    ++step
    cucumberMoved = moveFloor(floor)
    __DEBUG__ && drawFloor(floor, step)
    __DEBUG__ && (await readLine(''))
  }

  console.log('Final steps:', step)
}

main()
