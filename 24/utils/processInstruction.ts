import { Data, Instruction } from '../types'

export const processInstruction = (digits: number[], data: Data) => (instruction: Instruction) => {
  const [type, instructionNumber] = instruction

  if (type === 'up') {
    const digit = digits.shift()
    data.z = data.z * 26 + instructionNumber + digit
    data.result.push(digit)
  } else {
    const digit = (data.z % 26) + instructionNumber
    data.z = Math.trunc(data.z / 26)
    data.result.push(digit)

    if (digit < 1 || digit > 9) {
      return false
    }
  }

  return true
}
