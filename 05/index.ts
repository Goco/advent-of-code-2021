import { readInput } from '../utils/readInput'

const processLines = (lines: string[]): [[number, number], [number, number]][] => {
  return lines.map((line) => {
    const [point1, point2] = line.split(' -> ')
    const [x1, y1] = point1.split(',')
    const [x2, y2] = point2.split(',')

    return [
      [+x1, +y1],
      [+x2, +y2],
    ]
  })
}

const countOverlaps = (lines: [[number, number], [number, number]][], processDiagonals: boolean) => {
  const map: Record<string, number> = {}

  lines.forEach(([[x1, y1], [x2, y2]]) => {
    if (x1 === x2) {
      const x = x1
      const [yMin, yMax] = y1 <= y2 ? [y1, y2] : [y2, y1]

      for (let y = yMin; y <= yMax; ++y) {
        map[`${x}|${y}`] = (map[`${x}|${y}`] || 0) + 1
      }
    } else if (y1 === y2) {
      const y = y1
      const [xMin, xMax] = x1 <= x2 ? [x1, x2] : [x2, x1]

      for (let x = xMin; x <= xMax; ++x) {
        map[`${x}|${y}`] = (map[`${x}|${y}`] || 0) + 1
      }
    } else if (processDiagonals) {
      const xDelta = Math.abs(x1 - x2)
      const yDelta = Math.abs(y1 - y2)

      // is diagonal
      if (xDelta === yDelta) {
        const xGrow = x1 <= x2
        const yGrow = y1 <= y2

        for (let i = 0; i <= xDelta; ++i) {
          const x = xGrow ? x1 + i : x1 - i
          const y = yGrow ? y1 + i : y1 - i

          map[`${x}|${y}`] = (map[`${x}|${y}`] || 0) + 1
        }
      }
    }
  })

  return Object.values(map).filter((count) => count > 1).length
}

const main = () => {
  const inputLines = readInput()
  const lines = processLines(inputLines)

  const first = countOverlaps(lines, false)
  const second = countOverlaps(lines, true)

  console.log({ first, second })
}

main()
