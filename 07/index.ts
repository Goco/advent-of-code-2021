import { readInput } from '../utils/readInput'

const firstPart = (positions: number[]) => {
  const medianIndex = Math.round(positions.length / 2)
  const median = positions[medianIndex]

  return positions.reduce((acc, position) => {
    return acc + Math.abs(position - median)
  }, 0)
}

const secondPart = (positions: number[]) => {
  const positionsCount = positions.reduce((acc, position) => {
    return acc + position
  }, 0)

  const positionReduce = (middleValue: number) => (acc: number, position: number) => {
    const delta = Math.abs(position - middleValue)
    return acc + (1 + delta) * (delta / 2)
  }

  const middleValue1 = Math.trunc(positionsCount / positions.length)
  const middleValue2 = middleValue1 + 1

  const result1 = positions.reduce(positionReduce(middleValue1), 0)
  const result2 = positions.reduce(positionReduce(middleValue2), 0)

  return Math.min(result1, result2)
}

const main = () => {
  const lines = readInput()

  const positions = lines[0]
    .split(',')
    .map((n) => +n)
    .sort((a, b) => a - b)

  const first = firstPart(positions)
  const second = secondPart(positions)

  console.log({ first, second })
}

main()
