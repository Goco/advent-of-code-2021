export const getRawRangeFromRawCoordinates = (rawCoords: number[]): number[] =>
  Array.from(new Set(rawCoords)).sort((a, b) => a - b)
