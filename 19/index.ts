import { readInput } from '../utils/readInput'

type Position = [number, number, number]
type Scanner = {
  scanners: Position[]
  beacons: Position[]
}

const THRESHOLD = 12

const parseInput = (lines: string[]) => {
  const scanners: Scanner[] = []

  while (lines.length) {
    const scanner: Scanner = { scanners: [[0, 0, 0]], beacons: [] }
    // remove name of a scanner
    lines.shift()
    let beacon = lines.shift()
    while (!!beacon) {
      const position = beacon.split(',').map((position) => +position) as Position
      scanner.beacons.push(position)
      beacon = lines.shift()
    }
    scanners.push(scanner)
  }

  return scanners
}

const comparePositions = (c1: Position, c2: Position) => {
  return c1[0] === c2[0] && c1[1] === c2[1] && c1[2] === c2[2]
}

/**
 *  Permutation creator
 *  All you need to know to create the permutation options is that
 *  to rotate an object in some dimension you leave that dimension unchanged
 *  and switch those other directions + negate one of them.
 *  example: (x,y,z) -rotateX-> (x,z,-y)
 *  just brute-force these rotations to get 24 distinct options
 * @param x
 * @param y
 * @param z
 */
const getPositionPermutations = ([x, y, z]: Position): Position[] => {
  return [
    [x, y, z],
    [x, z, -y],
    [x, -y, -z],
    [x, -z, y],
    [y, x, -z],
    [y, z, x],
    [y, -x, z],
    [y, -z, -x],
    [z, x, y],
    [z, y, -x],
    [z, -x, -y],
    [z, -y, x],
    [-x, y, -z],
    [-x, z, y],
    [-x, -y, z],
    [-x, -z, -y],
    [-y, x, z],
    [-y, z, -x],
    [-y, -x, -z],
    [-y, -z, x],
    [-z, x, -y],
    [-z, y, x],
    [-z, -x, y],
    [-z, -y, -x],
  ]
}
const getScannerPermutations = (scanner: Scanner) => {
  let permutations: Scanner[] = new Array(24).fill({})
  permutations = permutations.map(() => ({ scanners: [], beacons: [] }))

  scanner.beacons.forEach((position) => {
    const positionPermutations = getPositionPermutations(position)
    positionPermutations.forEach((cp, i) => {
      permutations[i].beacons.push(cp)
    })
  })

  scanner.scanners.forEach((position) => {
    const positionPermutations = getPositionPermutations(position)
    positionPermutations.forEach((cp, i) => {
      permutations[i].scanners.push(cp)
    })
  })

  return permutations
}

const mergeScanners = (scanner1: Scanner, scanner2: Scanner): Scanner => {
  const newScanner: Scanner = { scanners: [...scanner1.scanners], beacons: [...scanner1.beacons] }

  scanner2.beacons.forEach((s2Position) => {
    if (scanner1.beacons.every((s1Position) => !comparePositions(s1Position, s2Position))) {
      newScanner.beacons.push(s2Position)
    }
  })

  scanner2.scanners.forEach((s2Scanner) => {
    newScanner.scanners.push(s2Scanner)
  })

  return newScanner
}

const moveScanner = (scanner: Scanner, transition: Position): Scanner => {
  return {
    scanners: scanner.scanners.map((position) => [
      position[0] + transition[0],
      position[1] + transition[1],
      position[2] + transition[2],
    ]),
    beacons: scanner.beacons.map((position) => [
      position[0] + transition[0],
      position[1] + transition[1],
      position[2] + transition[2],
    ]),
  }
}

const processScanners = (scanner1: Scanner, scanner2: Scanner): Scanner | undefined => {
  const scanner2Permutations = getScannerPermutations(scanner2)
  let mergedScanners: Scanner | undefined

  for (const scanner2Permutation of scanner2Permutations) {
    if (mergedScanners) {
      break
    }
    for (let i = 0; i < scanner1.beacons.length; ++i) {
      if (mergedScanners) {
        break
      }

      const s1c = scanner1.beacons[i]
      for (let j = i; j < scanner2Permutation.beacons.length; ++j) {
        if (mergedScanners) {
          break
        }

        const s2c = scanner2Permutation.beacons[j]
        const transition: Position = [s1c[0] - s2c[0], s1c[1] - s2c[1], s1c[2] - s2c[2]]

        const transitionedScanner2: Scanner = moveScanner(scanner2Permutation, transition)

        let count = 0
        scanner1.beacons.forEach((c1) =>
          transitionedScanner2.beacons.forEach((c2) => {
            if (comparePositions(c1, c2)) {
              ++count
            }
          })
        )

        if (count >= THRESHOLD) {
          mergedScanners = mergeScanners(scanner1, transitionedScanner2)
        }
      }
    }
  }

  return mergedScanners
}

const mergeAllScanners = (scanners: Scanner[]): Scanner => {
  let primaryScannersTested = 0
  let secondaryScannersTested = 0

  do {
    let primaryScanner = scanners.shift()!
    do {
      if (!scanners.length) {
        break
      }
      const secondaryScanner = scanners.shift()!
      const mergedScanner = processScanners(primaryScanner, secondaryScanner)

      if (mergedScanner) {
        primaryScanner = mergedScanner as Scanner
        secondaryScannersTested = 0
        primaryScannersTested = 0
      } else {
        secondaryScannersTested += 1
        scanners.push(secondaryScanner)
      }
    } while (secondaryScannersTested < scanners.length)
    scanners.push(primaryScanner)
    secondaryScannersTested = 0
    primaryScannersTested += 1
  } while (primaryScannersTested < scanners.length)

  return scanners[0]
}

const firstPart = (beacons: Position[]) => {
  return beacons.length
}
const secondPart = (scanners: Position[]) => {
  let biggestDistance = 0

  scanners.forEach((s1) => {
    scanners.forEach((s2) => {
      const distanceX = Math.abs(s1[0] - s2[0])
      const distanceY = Math.abs(s1[1] - s2[1])
      const distanceZ = Math.abs(s1[2] - s2[2])

      const distance = distanceX + distanceY + distanceZ

      if (distance > biggestDistance) {
        biggestDistance = distance
      }
    })
  })

  return biggestDistance
}

const main = () => {
  const lines = readInput()
  const scanners = parseInput(lines)

  const mergedScanner = mergeAllScanners(scanners)

  console.log(mergedScanner)

  const first = firstPart(mergedScanner.beacons)
  const second = secondPart(mergedScanner.scanners)

  console.log({ first, second })
}

main()
