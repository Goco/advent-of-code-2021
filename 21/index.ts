const POSITION_P1 = 8
const POSITION_P2 = 3

type Player = {
  score: number
  position: number
}
type Die = {
  throws: number
  roll: number
}

const dieRoll = (die: Die): number => {
  const roll = die.roll
  die.roll += 1
  if (die.roll > 100) {
    die.roll -= 100
  }
  die.throws += 1

  return roll
}
const movePlayer = (player: Player, roll: number) => {
  const realRoll = roll % 10
  let newPosition = player.position + realRoll
  if (newPosition > 10) {
    newPosition -= 10
  }

  player.position = newPosition
  player.score += newPosition
}
const firstPart = () => {
  const die = { throws: 0, roll: 1 }

  const player1 = { score: 0, position: POSITION_P1 }
  const player2 = { score: 0, position: POSITION_P2 }

  let loserScore

  while (true) {
    const roll1 = [dieRoll(die), dieRoll(die), dieRoll(die)].reduce((a, b) => a + b)
    movePlayer(player1, roll1)

    if (player1.score >= 1000) {
      loserScore = player2.score
      break
    }
    const roll2 = [dieRoll(die), dieRoll(die), dieRoll(die)].reduce((a, b) => a + b)
    movePlayer(player2, roll2)

    if (player2.score >= 1000) {
      loserScore = player1.score
      break
    }
  }

  return die.throws * loserScore
}

const ROLLS = [3, 4, 5, 6, 7, 8, 9]
const COUNTS = [1, 3, 6, 7, 6, 3, 1]
const findLowestScoreUniverse = (universeCount: Record<string, number>) => {
  let lowestKey: string | undefined
  let lowestScore: number | undefined

  Object.keys(universeCount).forEach((key) => {
    const [p1, p2] = key.split('|').map((n) => +n)
    const minScore = p1 + p2

    if (typeof lowestScore !== 'number' || minScore < lowestScore) {
      lowestScore = minScore
      lowestKey = key
    }
  })

  return `${lowestKey}`
}
const rollQuantumDie = ([s, p]: [number, number]): [number, number, number][] => {
  const games: [number, number, number][] = []
  ROLLS.forEach((roll, i) => {
    let position = roll + p
    if (position > 10) {
      position -= 10
    }
    games.push([s + position, position, COUNTS[i]])
  })

  return games
}
const filterWinningGames =
  (wins: { p1: number; p2: number }, key: 'p1' | 'p2', universeCount: number) =>
  ([s, , c]: [number, number, number]) => {
    if (s >= 21) {
      wins[key] += universeCount * c
      return false
    }
    return true
  }
const playQuantumGame = (universeKey: string, universeCounts: Record<string, number>) => {
  const wins = { p1: 0, p2: 0 }
  const universeCount = universeCounts[universeKey]
  const [s1, s2, p1, p2] = universeKey.split('|').map((n) => +n)
  const gamesP1 = rollQuantumDie([s1, p1]).filter(filterWinningGames(wins, 'p1', universeCount))
  const gamesP2 = rollQuantumDie([s2, p2]).filter(filterWinningGames(wins, 'p2', universeCount))

  gamesP1.forEach(([s1, p1, c1]) => {
    gamesP2.forEach(([s2, p2, c2]) => {
      const key = `${s1}|${s2}|${p1}|${p2}`
      universeCounts[key] = (universeCounts[key] || 0) + c1 * c2 * universeCount
    })
  })

  return wins
}
const secondPart = () => {
  // SCORE|SCORE|POSITION|POSITION
  const universeCounts: Record<string, number> = { [`0|0|${POSITION_P1}|${POSITION_P2}`]: 1 }

  const wins = { p1: 0, p2: 0 }
  while (Object.keys(universeCounts).length) {
    const universeKey = findLowestScoreUniverse(universeCounts)
    const { p1, p2 } = playQuantumGame(universeKey, universeCounts)
    wins.p1 += p1
    wins.p2 += p2
    delete universeCounts[universeKey]
  }

  return Math.max(wins.p1, wins.p2)
}

const main = () => {
  const first = firstPart()
  const second = secondPart()

  console.log({ first, second })
}

main()
