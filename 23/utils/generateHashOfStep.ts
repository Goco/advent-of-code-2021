import { Amphibian, Hash, Step } from '../types'

export const generateHashOfStep = (step: Step): Hash => [
  step[0],
  `${step[1].map(generateHashOfAmphibian).sort().join('-')}`,
]

const generateHashOfAmphibian = (amphibian: Amphibian): string => amphibian.join('.')
