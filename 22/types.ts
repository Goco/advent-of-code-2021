export type THREE_D<TYPE> = {
  x: TYPE
  y: TYPE
  z: TYPE
}

// STEP 1
export type Cuboid = THREE_D<[number, number]>

export type RebootStep = {
  operation: 'on' | 'off'
  cuboid: Cuboid
}

// STEP 2
export type Transformer = Record<number, number>
export type Transformer3D = THREE_D<Transformer>

export type Range = number[]
export type Range3D = THREE_D<Range>

export type Indexer = {
  transformer: Transformer3D
  range: Range3D
}

//STEP 3
export type Reactor = boolean[][][]
