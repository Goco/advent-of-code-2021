export type HERD_TYPE = '>' | 'v'

export type FLOOR_TILE = HERD_TYPE | '.'

export type FLOOR = FLOOR_TILE[][]
