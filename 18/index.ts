import { readInput } from '../utils/readInput'

type SnailNumber = [number | SnailNumber, number | SnailNumber]

const addNumber = (snailNumber: SnailNumber, add: { right?: number; left?: number }) => {
  const { left, right } = add
  if (left) {
    if (typeof snailNumber[0] === 'number') {
      snailNumber[0] += left
    } else {
      addNumber(snailNumber[0], add)
    }
  }
  if (right) {
    if (typeof snailNumber[1] === 'number') {
      snailNumber[1] += right
    } else {
      addNumber(snailNumber[1], add)
    }
  }
}

const checkForExplosion = (
  snailNumber: SnailNumber,
  depth: number
): { errorFound: boolean; explosion?: { right?: number; left?: number } } => {
  const leftNumber = snailNumber[0]
  const rightNumber = snailNumber[1]

  if (typeof leftNumber !== 'number') {
    if (depth === 4) {
      snailNumber[0] = 0

      const left = leftNumber[0] as number
      const right = leftNumber[1] as number

      if (typeof snailNumber[1] === 'number') {
        snailNumber[1] += right
      } else {
        addNumber(snailNumber[1], { left: right })
      }

      return {
        errorFound: true,
        explosion: {
          left,
        },
      }
    }

    const leftNumberProcess = checkForExplosion(leftNumber, depth + 1)
    const { errorFound, explosion } = leftNumberProcess
    if (errorFound) {
      const { left, right } = explosion ?? {}
      if (right) {
        if (typeof snailNumber[1] === 'number') {
          snailNumber[1] += right
        } else {
          addNumber(snailNumber[1], { left: right })
        }
      }
      return { errorFound, explosion: { left } }
    }
  }

  if (typeof rightNumber !== 'number') {
    if (depth === 4) {
      snailNumber[1] = 0

      const left = rightNumber[0] as number
      const right = rightNumber[1] as number

      if (typeof snailNumber[0] === 'number') {
        snailNumber[0] += left
      }

      return {
        errorFound: true,
        explosion: { right },
      }
    }

    const rightNumberProcess = checkForExplosion(rightNumber, depth + 1)
    const { errorFound, explosion } = rightNumberProcess
    if (rightNumberProcess.errorFound) {
      const { left, right } = explosion ?? {}
      if (left) {
        if (typeof snailNumber[0] === 'number') {
          snailNumber[0] += left
        } else {
          addNumber(snailNumber[0], { right: left })
        }
      }
      return { errorFound, explosion: { right } }
    }
  }

  return { errorFound: false }
}

const checkForSplit = (snailNumber: SnailNumber): { errorFound: boolean } => {
  const leftNumber = snailNumber[0]
  const rightNumber = snailNumber[1]

  if (typeof leftNumber === 'number') {
    if (leftNumber >= 10) {
      const halfNumber = Math.trunc(leftNumber / 2)
      const carryNumber = leftNumber % 2
      snailNumber[0] = [halfNumber, halfNumber + carryNumber]

      return { errorFound: true }
    }
  } else {
    const { errorFound } = checkForSplit(leftNumber)
    if (errorFound) return { errorFound }
  }

  if (typeof rightNumber === 'number') {
    if (rightNumber >= 10) {
      const halfNumber = Math.trunc(rightNumber / 2)
      const carryNumber = rightNumber % 2
      snailNumber[1] = [halfNumber, halfNumber + carryNumber]

      return { errorFound: true }
    }
  } else {
    const { errorFound } = checkForSplit(rightNumber)
    if (errorFound) {
      return { errorFound }
    }
  }

  return { errorFound: false }
}

const countMagnitude = (snailNumber: SnailNumber): number => {
  const leftNumber = snailNumber[0]
  const rightNumber = snailNumber[1]

  const leftPart = typeof leftNumber === 'number' ? leftNumber : countMagnitude(leftNumber)
  const rightPart = typeof rightNumber === 'number' ? rightNumber : countMagnitude(rightNumber)

  return 3 * leftPart + 2 * rightPart
}

const firstPart = (lines: string[]) => {
  const snailNumbers: SnailNumber[] = lines.map((line) => JSON.parse(line))

  let resultSnailNumber: SnailNumber = snailNumbers.shift()!

  while (snailNumbers.length) {
    const snailNumber = snailNumbers.shift()!
    resultSnailNumber = [resultSnailNumber, snailNumber]

    let errorFound
    do {
      const explosionCheck = checkForExplosion(resultSnailNumber, 1)
      errorFound = explosionCheck.errorFound || checkForSplit(resultSnailNumber).errorFound
    } while (errorFound)
  }

  return countMagnitude(resultSnailNumber)
}

const secondPart = (lines: string[]) => {
  let result = 0

  lines.forEach((line1, i) => {
    lines.forEach((line2, j) => {
      if (i === j) {
        return
      }

      const resultSnailNumber: SnailNumber = [JSON.parse(line1), JSON.parse(line2)]
      let errorFound
      do {
        const explosionCheck = checkForExplosion(resultSnailNumber, 1)
        errorFound = explosionCheck.errorFound || checkForSplit(resultSnailNumber).errorFound
      } while (errorFound)

      const magnitude = countMagnitude(resultSnailNumber)
      if (result < magnitude) {
        result = magnitude
      }
    })
  })

  return result
}

const main = () => {
  const lines = readInput()

  const first = firstPart(lines)
  const second = secondPart(lines)

  console.log({ first, second })
}

main()
