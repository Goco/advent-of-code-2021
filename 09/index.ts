import { readInput } from '../utils/readInput'

const firstPart = (heightMap: number[][]) => {
  let result = 0

  const maxI = heightMap.length - 1
  heightMap.forEach((line, i) => {
    const maxJ = line.length - 1

    line.forEach((point, j) => {
      const up = i > 0 ? heightMap[i - 1][j] : undefined
      const down = i < maxI ? heightMap[i + 1][j] : undefined
      const left = j > 0 ? heightMap[i][j - 1] : undefined
      const right = j < maxJ ? heightMap[i][j + 1] : undefined

      const neighbours = [up, down, left, right].filter((n) => n !== undefined)
      if (neighbours.every((n) => n! > point)) {
        result += point + 1
      }
    })
  })

  return result
}

type HeightPointsMap = Record<string, boolean>

const secondPart = (heightMap: number[][]) => {
  const heightPointsMap = {} as HeightPointsMap
  const topThreeBasins = [0, 0, 0]
  const searchList: [number, number][] = []

  const maxI = heightMap.length - 1
  heightMap.forEach((line, i) => {
    const maxJ = line.length - 1

    line.forEach((point, j) => {
      const key = `${i}|${j}`

      if (point === 9 || heightPointsMap[key]) {
        return
      }

      let result = 0
      searchList.push([i, j])
      heightPointsMap[key] = true

      while (searchList.length) {
        result += 1
        const [si, sj] = searchList.shift()!

        const up = si > 0 ? [si - 1, sj] : undefined
        const down = si < maxI ? [si + 1, sj] : undefined
        const left = sj > 0 ? [si, sj - 1] : undefined
        const right = sj < maxJ ? [si, sj + 1] : undefined

        const neighbours = [up, down, left, right]

        neighbours.forEach((neighbour) => {
          if (!!neighbour) {
            const [ni, nj] = neighbour
            const nKey = `${ni}|${nj}`
            if (!(heightMap[ni][nj] === 9 || heightPointsMap[nKey])) {
              searchList.push([ni, nj])
              heightPointsMap[nKey] = true
            }
          }
        })
      }

      const position = topThreeBasins.findIndex((b) => b < result)
      if (position !== -1) {
        topThreeBasins.splice(position, 0, result)
        topThreeBasins.pop()
      }
    })
  })

  return topThreeBasins.reduce((a, b) => a * b, 1)
}

const main = () => {
  const lines = readInput()
  const heightMap = lines.map((line) => line.split('').map((n) => +n))

  const first = firstPart(heightMap)
  const second = secondPart(heightMap)

  console.log({ first, second })
}

main()
